function BookList(props) {
    return (
      <ul>
      {
          props.books.map((book, index) =>
            <li>
              <input type="checkbox" checked={book.deleteFlag} onClick={() => props.delete(index)} />{book.title}
            </li>
          )
      }
      </ul>
    );
  }
  
  export default BookList;
  