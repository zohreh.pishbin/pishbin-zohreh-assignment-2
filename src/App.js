import { useState } from 'react';
import './App.css';
import List from './BookList';

function App() {

  let [books, setBooks] = useState([
    {deleteFlag: false, title:"The Bible"},
    {deleteFlag: false, title:"Where the Red Fern Grows"},
    {deleteFlag: false, title:"Robinson Crusoe"}
  ]);

  let [newTitle, setNewTitle] = useState('');

  function addBook() {
    if(!newTitle) return;
    const newBooks = books.map(book => {
      return {...book}
    });
    setBooks([...books, {deleteFlag: false, title: newTitle}]);
    setNewTitle("");
  }

  function deleteToggle(i) {
    const newBooks = books.map(book => {
      return {...book}
    });
    newBooks[i].deleteFlag = !newBooks[i].deleteFlag;
    setBooks(newBooks);
  }

  function deleteBooks() {
    let newBooks = books.map(book => {
      return {...book}
    });
    newBooks = newBooks.filter(book => book.deleteFlag === false);
    setBooks(newBooks);
  }

  const deleteDisabled = books.reduce((acc, book) => book.deleteFlag === false && acc, true);

  return (
    <div className="App">
      <div>
        <button onClick={addBook}>Add</button>
        <button disabled={deleteDisabled} onClick={deleteBooks}>Delete</button>
      </div>
      <div>
        <input type="text" value={newTitle} onChange={e=>setNewTitle(e.target.value)}></input>
      </div>
      <List books={books} delete={deleteToggle}></List>
    </div>
  );
}

export default App;
